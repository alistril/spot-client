import requests
from . import spot

class GoogleNamer(object):
    def __init__(self, settings):
        self.settings = settings        
        self.FULL_API_URL = self.settings.get("API_URL", "http://maps.googleapis.com/maps/api/geocode/json")
    
    def get_subkey_matching_type(self,addr_type,stuff,subkey):
        if "types" in stuff:
            for t in stuff["types"]:
                if addr_type == t:
                    return stuff[subkey]
        return None
    
    def find_type_in_results(self, t,results):
        
        for result in results:            
            ret = self.get_subkey_matching_type(t,result,"formatted_address")
            if ret:
                return ret
            else:                
                if "address_components" in result:                    
                    for address_component in result["address_components"]:
                        ret = self.get_subkey_matching_type(t,address_component,"long_name")                        
                        if ret:
                            return ret
                    
        return None
            

    def add_names(self, messages):
        addr_types= ["city", "town", "locality", "postal_town", "country", "political"]
                     
        for message in messages:
            r = requests.get(self.FULL_API_URL, params={
                                                        "latlng" : "%f,%f" % (message.position.x,message.position.y),
                                                        "sensor" : "false"
                                                        })
            if r.status_code != 200:
                raise spot.UnexpectedResponse()
            
            data = r.json()
            
            message.name = "Unknown"
            if data["status"]!="OK":                
                continue
            else:
                for addr_type in addr_types:
                    name = self.find_type_in_results(addr_type,data["results"])                    
                    if name:
                        message.name = name
                        break