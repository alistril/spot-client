import requests
from shapely.geometry import Point
from shapely.wkt import dumps

import datetime
import pytz

class UnexpectedResponse(Exception):
    pass

class SPOTMessageType:
    OK      = 1
    CUSTOM  = 2
    HELP    = 3
    SOS     = 4
    
    SPOTCONNECT = "SPOTCONNECT"
    
    string_types = { 
                    "OK"       : OK,
                    "CUSTOM"   : CUSTOM,
                    "HELP"     : HELP,
                    "SOS"      : SOS
                    }
    
    type_strings = { 
                    OK       : "OK",
                    CUSTOM   : "CUSTOM",
                    HELP     : "HELP",
                    SOS      : "SOS"
                    }
    
    battery_types = { 
                    "GOOD"       : 1,
                    "LOW"        : 2
                    }
    
class SPOTMessage(object):
    def __init__(self, data):        
        self.id             = data['id']
        self.modelId        = data['modelId']
        self.messengerId    = data['messengerId']
        self.messengerName  = data['messengerName']
        self.messageType    = SPOTMessageType.string_types[data['messageType']]
        self.position       = Point(data['latitude'],data['longitude'])
        self.datetime       = datetime.datetime.fromtimestamp(data['unixTime'],pytz.utc)
        self.name           = None
        self.content        = data['messageContent']
        if 'batteryState' in data:
            self.battery_state  = SPOTMessageType.battery_types[data['batteryState']]
        else:
            self.battery_state = None
        
    def __str__(self):
        return "%s(%s){date: %s, pos: %s}" % (
                                                     SPOTMessageType.type_strings[self.messageType],                                                      
                                                     self.messengerName,
                                                     self.datetime,                                                     
                                                     self.name or dumps(self.position)
                                                     )
    def __unicode__(self):
        return self.__str__()
    
    def __repr__(self):
        return self.__str__()
        
    
class SPOTMessages(object):
    def __init__(self, full_api_url, settings):
        self.FULL_API_URL = full_api_url
        
    def all_available(self):
        r = requests.get(self.FULL_API_URL)
        
        if r.status_code != 200:
            raise UnexpectedResponse()
        data = r.json()
        if "message" in data["response"]["feedMessageResponse"]["messages"]:            
            messages = data["response"]["feedMessageResponse"]["messages"]["message"]
            if isinstance(messages,dict): #single element case
                messages = [messages]            
        else:            
            messages = data["response"]["feedMessageResponse"]["messages"]
        
        return [SPOTMessage(message) for message in messages]
    
    def before_moment(self,moment):
        return [SPOTMessage(message) for message in self.all_available() if message.datetime < moment]
    
    def after_moment(self,moment):
        return [message for message in self.all_available() if message.datetime > moment]
        
class SPOTClient(object):
    def __init__(self, settings):
        self.settings = settings        
        self.FULL_API_URL = self.settings.get("FULL_API_URL", "%s%s/message.json" % (self.settings["API_URL"],self.settings["FEED_ID"]))

        self.messages = SPOTMessages(self.FULL_API_URL,self.settings)

