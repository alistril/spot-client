import spot
import google
import datetime
import pytz

settings = {
            "SPOT" : {
                      "API_URL" : "https://api.findmespot.com/spot-main-web/consumer/rest-api/2.0/public/feed/",
                      "FEED_ID" : "03Cc3p8bbZqsJZZyJsAXn0inqncWDONdr",
                      },
            "GOOGLE" : {
                        "API_URL" : "http://maps.googleapis.com/maps/api/geocode/json"
                        }
            }

print "sending request...."
sp = spot.SPOTClient(settings["SPOT"])

messages = sp.messages.after_moment(datetime.datetime(2012, 03, 31,13,43,18,0,pytz.UTC))
gn = google.GoogleNamer(settings["GOOGLE"])
gn.add_names(messages)
print messages

