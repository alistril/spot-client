1) Configure a settings dict in the following way:
settings = {
            "SPOT" : {
                      "API_URL" : "https://api.findmespot.com/spot-main-web/consumer/rest-api/2.0/public/feed/",
                      "FEED_ID" : "????",
                      },
            "GOOGLE" : {
                        "API_URL" : "http://maps.googleapis.com/maps/api/geocode/json"
                        }
            }

2) 
sp = spot.SPOTClient(settings["SPOT"])

messages = sp.messages.all_available()
gn = google.GoogleNamer(settings["GOOGLE"])
gn.add_names(messages) #add readable name (city or sth) to coordinates

