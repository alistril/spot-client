try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


setup(
    name='spot-client',
    version="0.5",
    description="Client for SPOT GPS",
    long_description="Dls the gps coordinate from spot rest api",
    author="Alistril",
    author_email="alistril@gmail.com",
    url="https://bitbucket.org/alistril/spot_client",
    license="MIT License",
    platforms=["any"],
    packages=['spot_client'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Framework :: Django",
    ],
    include_package_data=True,
)
